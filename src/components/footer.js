// import { Link } from "gatsby"
import React from 'react';

const Footer = () => (
    <div className='footer'>
        <p>FOSS NSS &copy; 2019</p>
    </div>
);

export default Footer;
